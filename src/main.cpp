#include "../include/operations.h"
#include <iostream>
#include <string>
#include <cmath>


double coord::input() {
    double input;
    std::cin >> input;
    while(std::cin.fail()) {
        std::cout << "invalid input. Try again: ";
        std::cin.clear();
        std::cin.ignore();
        std::cin >> input;
    }
    return input;
}

void coord::set() {
    std::cout << "Input x: ";
    x = input();
    std::cout << "Input y: ";
    y = input();
}

std::string coord::get() {
    return ("(" + std::to_string(x) + ", " + std::to_string(y) + ")");
}

bool coord::equal(coord& other) {
    if(x == other.x && y == other.y) {
        std::cout << "start and end can't be equal\n";
        return false;
    }
    return true;
}
    
bool coord::compare(coord& other){
    if (x == other.x && y == other.y){
        return true;
    }
    return false;
}

bool line::inLine(coord& other){
    double left = (other.x - start.x) / (end.x - start.x);
    double right = (other.y - start.y) / (end.y - start.y);
    double delta = 0.0001;
    if(fabs(left - right) <= delta) {
        return true;
    }
    return false;
}
    
bool line::compare(line& other){
    if (start.compare(other.start) && end.compare(other.end)){
        return true;
    }
    std::cout << "Joint failed. Invalid coordinates\n";
    return false;
}

void scalpel(coord& start, coord& end){
    std::cout << "Make incision: " << start.get();
    std::cout << " -> " << end.get() << std::endl;
}

void hemostat(coord& point){
    std::cout << "Use hemostat at: " << point.get() << std::endl;
}

void tweezers(coord& point){
    std::cout << "Use tweezers at: " << point.get() << std::endl;
}

void suture(coord& start, coord& end){
    std::cout << "Make joint: " << start.get();
    std::cout << " -> " << end.get() << std::endl;
}



int main(int argc, char** argv)
{
    line incision;
    coord operation;
    std::string input;
    do{
        std::cout << "Input incision coordinates\n";
        std::cout << "Start:\n";
        incision.start.set();
        std::cout << "End:\n";
        incision.end.set();
    } while (!incision.start.equal(incision.end));
    scalpel(incision.start, incision.end);

    while(input != "end") {
        std::cout << "Input operation or 'end' to finish: ";
        std::cin >> input;
        if(input == "hemostat") {
            std::cout << "Input coordinates\n";
            operation.set();
            while (!incision.inLine(operation)){
                std::cout << "Invalid coordinates. Try again\n";
                operation.set();
            }
            hemostat(operation);
        } else if(input == "tweezers") {
            std::cout << "Input coordinates\n";
            operation.set();
            while (!incision.inLine(operation)){
                std::cout << "Invalid coordinates. Try again\n";
                operation.set();
            }
            tweezers(operation);
        } else if(input != "end") {
            std::cout << "Invalid operation\n";
        }
    }
    
    line joint;
    do{
    std::cout << "Input joint coordinates:\n";
    std::cout << "Start:\n";
    joint.start.set();
    std::cout << "End:\n";
    joint.end.set();
    } while (!incision.compare(joint));
    suture(incision.start, incision.end);

    return 0;
}