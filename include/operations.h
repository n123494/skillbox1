#pragma once
#include <cmath>
#include <iostream>

struct coord {
    double x = 0;
    double y = 0;

    double input();

    void set();

    std::string get();

    bool equal(coord& other);
    
    bool compare(coord& other);
};

struct line {
    coord start;
    coord end;

    bool inLine(coord& other);
    
    bool compare(line& other);
};

void scalpel(coord& start, coord& end);

void hemostat(coord& point);

void tweezers(coord& point);

void suture(coord& start, coord& end);
